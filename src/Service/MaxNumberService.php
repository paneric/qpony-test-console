<?php

declare(strict_types=1);

namespace QPOny\Console\Service;

class MaxNumberService
{
    public function find(string $sequence): ?string
    {
        $sequence = array_filter(
            explode(',',$sequence),
            static function($nbr) {
                return ctype_digit($nbr) && (0 < (int) $nbr && (int) $nbr < 100000);
            }
        );

        $sequence = array_splice(array_unique($sequence), 0, 10);

        if (empty($sequence)) {
            return null;
        }

        return max($sequence);
    }
}
