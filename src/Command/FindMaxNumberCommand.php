<?php

declare(strict_types=1);

namespace QPOny\Console\Command;

use QPOny\Console\Service\MaxNumberService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Paneric\CSRConsole\Service\CService;
use Paneric\CSRConsole\Service\SService;
use Paneric\CSRConsole\Service\RService;
use Paneric\CSRConsole\Service\REService;
use Paneric\CSRConsole\Service\RIService;
use Paneric\CSRConsole\Service\CSRService;

class FindMaxNumberCommand extends Command
{
    protected static $defaultName = 'max';

    protected $maxNumberService;

    public function __construct(MaxNumberService $maxNumberService)
    {
        $this->maxNumberService = $maxNumberService;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Finds max number within a sequence.')

            ->setHelp('This command allows you to find a max number within a given sequence.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $questionHelper = $this->getHelper('question');
        $sequence = $questionHelper->ask(
            $input,
            $output,
            new Question('Enter coma-separated sequence of natural numbers up to 10 (1 - 99999): ')
        );

        $this->showOutput(
            $sequence,
            $output,
            $this->maxNumberService->find($sequence)
        );

        return 0;
    }

    protected function showOutput(string $sequence, OutputInterface $output, string $maxNumber = null): void
    {
        if ($maxNumber === null) {
            $output->getFormatter()->setStyle(
                'title',
                new OutputFormatterStyle('white', 'red', ['bold'])
            );

            $output->writeln([
                '',
                '<title>                                                                          </>',
                '<title>  INVALID SEQUENCE ELEMENTS.                                              </>',
                '<title>                                                                          </>',
                ''
            ]);

            return;
        }
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('white', 'green', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  MAX VALUE OF SEQUENCE ELEMENTS FOUND.                                   </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  sequence: %s. </>',
                $sequence
            ),
            '',
            sprintf(
                '<options=bold>  max value: %s. </>',
                $maxNumber
            ),
            '',
        ]);

    }
}
