<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;

use Paneric\CSRConsole\Service\RIService;
use Paneric\CSRConsole\Service\REService;
use Paneric\CSRConsole\Service\RService;
use Paneric\CSRConsole\Service\SService;
use Paneric\CSRConsole\Service\CService;
use Paneric\CSRConsole\Service\CSRService;

use Paneric\CSRConsole\Service\DTO\DTOService;
use Paneric\CSRConsole\Service\DTO\DTOStatementService;

return [

    RService::class => static function (ContainerInterface $container)
    {
        return new RService(
            (string) $container->get('app_folder'),
            $container->get('csr_templates')['r']
        );
    },

    REService::class => static function (ContainerInterface $container)
    {
        return new REService(
            (string) $container->get('app_folder'),
            $container->get('csr_templates')['re']
        );
    },

    RIService::class => static function (ContainerInterface $container)
    {
        return new RIService(
            (string) $container->get('app_folder'),
            $container->get('csr_templates')['ri']
        );
    },

    SService::class => static function (ContainerInterface $container)
    {
        return new SService(
            (string) $container->get('app_folder'),
            $container->get('csr_templates')['s']
        );
    },

    CService::class => static function (ContainerInterface $container)
    {
        return new CService(
            (string) $container->get('app_folder'),
            $container->get('csr_templates')['c']
        );
    },

    CSRService::class => static function (ContainerInterface $container)
    {
        return new CSRService(
            $container->get(CService::class),
            $container->get(SService::class),
            $container->get(RService::class),
            $container->get(RIService::class)
        );
    },

    DTOStatementService::class => static function (ContainerInterface $container)
    {
        return new DTOStatementService();
    },

    DTOService::class => static function (ContainerInterface $container)
    {
        return new DTOService(
            (string) $container->get('app_folder'),
            $container->get('csr_templates')['dto'],
            $container->get(DTOStatementService::class)
        );
    },

];
